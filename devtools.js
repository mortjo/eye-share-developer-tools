chrome.devtools.panels.elements.createSidebarPane("eye-share config",
    function(sidebar) {
        chrome.devtools.panels.elements.onSelectionChanged.addListener(function () {
            sidebar.setExpression("(" + getConfig.toString() + ")()");
        });
    });

function getConfig() {
    if (!window.runtimeFlags.debug)
        return { "Error": "Debugging is disabled in eye-share"};

    if ($0) {
        var field = $($0).closest(".es-field");
        var scope = window.angular.element(field).scope();
        var path = [];
        path.push(getName("f", field));
        var panel = field.parent();
        while (panel.hasClass("es-panel")){
            path.push(getName("p", panel));
            panel = panel.parent();
        }
        path.push(scope.formName);

        var config = scope._pathedCfg;
        for(var i = path.length-1; i > -1; i--){
            config = config[path[i]]
        }
        return config;
    }

    function getName(prefix, el) {
        return Array.prototype.find.call(el[0].classList, function (cls) {
            return cls.startsWith(prefix + "-");
        }).substring(2);
    }
}

chrome.devtools.panels.create("eye-share", "icon.png", "panel.html", function(panel) { });



