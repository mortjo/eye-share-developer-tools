var response = document.getElementById('response');
document.getElementById('clearRedisCache').addEventListener('click', function() {
    callMethod('$.connection.system.server.clearRedisCache()', 'Cached cleared');
});

document.getElementById('generateAssets').addEventListener('click', function() {
    callMethod('$.connection.system.server.updateAssets()', 'Assets updated');
});

function callMethod(method, successMsg) {
    chrome.devtools.inspectedWindow.eval(method,
        function(result, isException) {
            if (isException){
                response.innerHTML = "Something went wrong";
            }
            else{
                result.then(function (response) {
                    response.innerHTML = successMsg;
                });
            }
        });
}